//
//  AppDelegate.swift
//  TwistSearch
//
//  Created by Oscar Swanros on 12/28/17.
//  Copyright © 2017 Doist. All rights reserved.
//

import Cocoa
import TwistAPI

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        TwistEndpoint.environment = .staging
        
        let session = SessionManager.shared.currentSession
        var initialWindowController: NSWindowController
        
        if session != nil {
            initialWindowController = SearchWindowController()
        } else {
            initialWindowController = LogInWindowController()
        }
        
        initialWindowController.showWindow(nil)
        initialWindowController.window?.makeKeyAndOrderFront(true)
        initialWindowController.window?.center()
    }
}

