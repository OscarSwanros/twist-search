//
//  LoginWindowController.swift
//  TwistSearch
//
//  Created by Oscar Swanros on 12/28/17.
//  Copyright © 2017 Doist. All rights reserved.
//

import Cocoa

private extension NSNib.Name {
    static let logInWindow = NSNib.Name(rawValue: "LogInWindow")
}

public final class LogInWindowController: NSWindowController {
    public init() {
        let window = LogInWindow()
        
        super.init(window: window)
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
