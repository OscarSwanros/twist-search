//
//  LogInWindow.swift
//  TwistSearch
//
//  Created by Oscar Swanros on 12/28/17.
//  Copyright © 2017 Doist. All rights reserved.
//

import Cocoa
import TwistAPI

private extension Selector {
    static let signInClicked = #selector(LogInWindow.signInClicked)
}

final class LogInWindow: NSWindow {
    private let queue = OperationQueue()
    
    private lazy var imageView: NSImageView = {
        let i = NSImageView()
        i.translatesAutoresizingMaskIntoConstraints = false
        i.image = #imageLiteral(resourceName: "twist_logo")
        return i
    }()
    
    private lazy var emailTextField: NSTextField = {
        let t = NSTextField()
        t.translatesAutoresizingMaskIntoConstraints = false
        t.placeholderString = "Email"
        return t
    }()
    
    private lazy var passwordTextField: NSSecureTextField = {
        let s = NSSecureTextField()
        s.translatesAutoresizingMaskIntoConstraints = false
        s.placeholderString = "Password"
        return s
    }()
    
    private lazy var signInButton: NSButton = {
        let b = NSButton(title: "Sign In", target: self, action: .signInClicked)
        b.translatesAutoresizingMaskIntoConstraints = false
        return b
    }()
    
    
    init() {
        super.init(contentRect: NSMakeRect(0, 0, 250, 200),
                   styleMask: [.closable, .titled],
                   backing: .buffered,
                   defer: true)
        
        setUp()
    }
    
    private func setUp() {
        title = "Sign In to Twist"
        
        contentView?.addSubview(imageView)
        contentView?.addSubview(emailTextField)
        contentView?.addSubview(passwordTextField)
        contentView?.addSubview(signInButton)
        
        guard let view = contentView else { return }
        
        NSLayoutConstraint.activate([
            
            imageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 20),
            imageView.widthAnchor.constraint(equalToConstant: 100),
            imageView.heightAnchor.constraint(equalToConstant: 100),
            imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            emailTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            emailTextField.widthAnchor.constraint(equalToConstant: 250),
            emailTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            emailTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            emailTextField.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 20),
            
            passwordTextField.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant: 10),
            passwordTextField.leadingAnchor.constraint(equalTo: emailTextField.leadingAnchor),
            passwordTextField.trailingAnchor.constraint(equalTo: emailTextField.trailingAnchor),
            
            signInButton.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 20),
            signInButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            signInButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20),
            ])
    }
    
    @objc
    fileprivate func signInClicked() {
        let op = LogInOperation(
            email: emailTextField.stringValue,
            password: passwordTextField.stringValue
        )
        op.add(observer: LogInObserver(window: self))
        queue.addOperation(op)
    }
}

private struct LogInObserver: OperationObserver {
    private weak var window: LogInWindow?
    
    init(window: LogInWindow) {
        self.window = window
    }
    
    func operationDidFinish(operation: TOperation) {
        guard
            let op = operation as? LogInOperation,
            let session = op.generatedModel
            else {
                return
        }
        
        do {
            try SessionManager.shared.save(session: session)
            
            DispatchQueue.main.async {
                self.window?.close()
                
                let c = SearchWindowController()
                c.showWindow(nil)
                c.window?.makeKeyAndOrderFront(nil)
                c.window?.center()
            }
            
        } catch {
            self.operation(operation: operation, didFinishWithErrors: [error])
        }
    }
    
    func operation(operation: TOperation, didFinishWithErrors errors: [Error]) {
        guard let error = errors.first else {
            return
        }
        
        DispatchQueue.main.async {
            let alert = NSAlert(error: error)
            alert.addButton(withTitle: "Ok")
            alert.runModal()
        }
    }
}


