//
//  SearchWindowController.swift
//  TwistSearch
//
//  Created by Oscar Swanros on 12/28/17.
//  Copyright © 2017 Doist. All rights reserved.
//

import Cocoa

final class SearchWindowController: NSWindowController {
    let queue = OperationQueue()
    
    init() {
        let window = SearchWindow()
        super.init(window: window)
        window.windowController = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
