//
//  SearchResultRowView.swift
//  TwistSearch
//
//  Created by Oscar Swanros on 12/28/17.
//  Copyright © 2017 Doist. All rights reserved.
//

import Cocoa
import TwistAPI

final class SearchResultRowView: NSTableCellView {
    private lazy var titleLabel: NSTextField = {
        let t = NSTextField()
        t.translatesAutoresizingMaskIntoConstraints = false
        t.isEditable = false
        t.maximumNumberOfLines = 2
        t.lineBreakMode = .byWordWrapping
        t.font = NSFont.boldSystemFont(ofSize: 13)
        t.isBordered = false
        return t
    }()
    
    private lazy var snippetLabel: NSTextField = {
        let t = NSTextField()
        t.translatesAutoresizingMaskIntoConstraints = false
        t.isEditable = false
        t.maximumNumberOfLines = 5
        t.lineBreakMode = .byWordWrapping
        t.font = NSFont.systemFont(ofSize: 13)
        t.isBordered = false
        t.usesSingleLineMode = false
        t.cell?.wraps = true
        t.cell?.isScrollable = false
        return t
    }()
    
    private lazy var typeLabel: NSTextField = {
        let t = NSTextField()
        t.translatesAutoresizingMaskIntoConstraints = false
        t.isEditable = false
        t.font = NSFont.boldSystemFont(ofSize: 10)
        t.isBordered = true
        t.wantsLayer = true
        t.layer?.cornerRadius = 7
        t.alignment = .center
        return t
    }()
    
    override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        
        commonInit()
    }
    
    required init?(coder decoder: NSCoder) {
        fatalError()
    }
    
    private func commonInit() {
        addSubview(titleLabel)
        addSubview(snippetLabel)
        addSubview(typeLabel)
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 5),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            
            snippetLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor),
            snippetLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            snippetLabel.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor),
            
            typeLabel.topAnchor.constraint(equalTo: snippetLabel.bottomAnchor, constant: 3),
            typeLabel.leadingAnchor.constraint(equalTo: snippetLabel.leadingAnchor),
            typeLabel.heightAnchor.constraint(equalToConstant: 15),
            typeLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5),
            ])
    }
    
    func configure(item: SearchItem) {
        titleLabel.stringValue = item.title ?? "[No Title]"
        snippetLabel.stringValue = item.snippet
        typeLabel.stringValue = item.type.rawValue
    }
}
