//
//  SearchWindowContentViewController.swift
//  TwistSearch
//
//  Created by Oscar Swanros on 12/28/17.
//  Copyright © 2017 Doist. All rights reserved.
//

import Cocoa
import TwistAPI

private extension Selector {
    static let kickSearch = #selector(SearchWindowContentViewController.kickSearch)
    static let workspacesStoreUpdated = #selector(SearchWindowContentViewController.workspacesStoreUpdated)
}

final class SearchWindowContentViewController: NSViewController {
    let queue = OperationQueue()
    
    private lazy var searchField: NSSearchField = {
        let s = NSSearchField()
        s.translatesAutoresizingMaskIntoConstraints = false
        s.delegate = self
        return s
    }()
    
    private lazy var tableView: NSTableView = {
        let t = NSTableView()
        t.translatesAutoresizingMaskIntoConstraints = false
        t.setContentHuggingPriority(.defaultLow, for: .vertical)
        t.dataSource = self
        t.delegate = self
        t.headerView = nil
        t.intercellSpacing = NSSize(width: 0, height: 0.2)
        t.gridColor = NSColor.black
        let col = NSTableColumn(identifier: NSUserInterfaceItemIdentifier(rawValue: "results"))
        
        t.addTableColumn(col)
        
        return t
    }()
    
    private lazy var scrollView: NSScrollView = {
        let scrollView = NSScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.hasVerticalScroller = true
        scrollView.documentView = tableView
        
        return scrollView
    }()
    
    fileprivate var searchResultItems: [SearchItem] = [] {
        didSet {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    fileprivate var lastTitle: String = ""
    
    override func loadView() {
        self.view = NSView()
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate var searchTimer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self,
                                               selector: .workspacesStoreUpdated,
                                               name: .workspacesStoreUpdated,
                                               object: nil)
        
        view.addSubview(searchField)
        view.addSubview(scrollView)
        
        NSLayoutConstraint.activate([
            searchField.topAnchor.constraint(equalTo: view.topAnchor, constant: 5),
            searchField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 5),
            searchField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -5),
            searchField.widthAnchor.constraint(greaterThanOrEqualToConstant: 280),
            
            scrollView.topAnchor.constraint(equalTo: searchField.bottomAnchor, constant: 5),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            
            view.heightAnchor.constraint(greaterThanOrEqualToConstant: 360)
            ])
        
        configureContextualInformation()
        
        if let lastResults = SearchResultsController.shared.lastResults {
            searchResultItems = lastResults.items
        }
    }
    
    @objc
    fileprivate func workspacesStoreUpdated() {
        configureContextualInformation()
    }
    
    private func configureContextualInformation() {
        searchField.placeholderString = "Search in \(WorkspaceManager.shared.currentWorkspace?.name ?? "workspace")"
    }
}

extension SearchWindowContentViewController: NSSearchFieldDelegate {
    override func controlTextDidChange(_ obj: Notification) {
        guard searchField.stringValue != "" else { return }
        
        searchTimer?.invalidate()
        searchTimer = nil
        searchTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: .kickSearch, userInfo: nil, repeats: false)
    }
    
    @objc
    fileprivate func kickSearch() {
        lastTitle = view.window?.title ?? ""
        view.window?.title = "Loading..."
        
        queue.cancelAllOperations()
        
        let op = SearchTermOperation(term: searchField.stringValue)
        op.add(observer: SearchTermOperationObserver(owner: self))
        queue.addOperation(op)
    }
}

private struct SearchTermOperationObserver: OperationObserver {
    weak var owner: SearchWindowContentViewController?
    
    init(owner: SearchWindowContentViewController) {
        self.owner = owner
    }
    
    func operationDidFinish(operation: TOperation) {
        guard
            let op = operation as? SearchTermOperation,
            let res = op.results
            else {
                return
        }
        
        DispatchQueue.main.async {
            self.owner?.view.window?.title = self.owner?.lastTitle ?? ""
            self.owner?.searchResultItems = res.items
        }
    }
    
    func operation(operation: TOperation, didFinishWithErrors errors: [Error]) {
    }
}

extension SearchWindowContentViewController: NSTableViewDataSource {
    func numberOfRows(in tableView: NSTableView) -> Int {
        return searchResultItems.count
    }
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        let id = "CellIdentifier"
        
        var cell = tableView.makeView(withIdentifier: .init(rawValue: id), owner: self) as? SearchResultRowView
        
        if cell == nil {
            cell = SearchResultRowView(frame: NSMakeRect(0, 0, tableView.frameOfCell(atColumn: 0, row: row).width, 60))
            cell?.identifier = .init(rawValue: id)
        }
        
        cell?.configure(item: searchResultItems[row])
        
        return cell
    }
    
    func tableView(_ tableView: NSTableView, shouldSelectRow row: Int) -> Bool {
         return false
    }
}

extension SearchWindowContentViewController: NSTableViewDelegate {
    func tableView(_ tableView: NSTableView, heightOfRow row: Int) -> CGFloat {
        return 80
    }
}
