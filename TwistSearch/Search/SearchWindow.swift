//
//  SearchWindow.swift
//  TwistSearch
//
//  Created by Oscar Swanros on 12/28/17.
//  Copyright © 2017 Doist. All rights reserved.
//

import Cocoa
import TwistAPI


final class SearchWindow: NSWindow {
    let viewController = SearchWindowContentViewController()
    let queue = OperationQueue()
    
    init() {
        super.init(contentRect: NSMakeRect(0, 0, 280, 360),
                   styleMask: [.closable, .miniaturizable, .resizable, .titled],
                   backing: .buffered,
                   defer: false)
        
        contentView = viewController.view
        
        setUp()
    }
    
    private func setUp() {
        configureWindow()
        
        let op = GetWorkspacesOperation()
        op.add(observer: GetWorkspacesOperationObserver(owner: self))
        queue.addOperation(op)
    }
    
    fileprivate func configureWindow() {
        title = WorkspaceManager.shared.currentWorkspace?.name ?? "Twist"
    }
}

struct GetWorkspacesOperationObserver: OperationObserver {
    private weak var owner: SearchWindow?
    
    init(owner: SearchWindow) {
        self.owner = owner
    }
    
    func operationDidFinish(operation: TOperation) {
        guard
            let op = operation as? GetWorkspacesOperation,
            let w = op.generatedModel
            else {
                return
        }
        
        try? WorkspaceManager.shared.save(workspaces: w)
        
        owner?.configureWindow()
    }
    
    func operation(operation: TOperation, didFinishWithErrors errors: [Error]) {
    }
}
