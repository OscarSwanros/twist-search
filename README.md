## Twist API Search

This application is a basic search results display implementation usign the Twist API v2.

![Twist API Search](https://dha4w82d62smt.cloudfront.net/items/1Y3w2O2c37071m05122E/Screen%20Recording%202017-12-28%20at%2011.39%20PM.gif)

### Notes

- In `AppDelegate.swift`, you can set the environment the application points to. It defaults to `.production`.
- The app uses no external dependencies whatsoever.
- `TwistAPI` is the module where everything related to data management lives. The app target should only be concerned about UI-level details, such as presenting the information retrieved from the API.
- I went with a operation-oriented architecture. This architecture will let the developer build on top of something that's really modular and concise. For instance, the implementation to get the user's workspaces is just 3 lines long (see `GetWorkspacesOperation.swift`).
- I also decided not to use NIBs or Storyboards to define the UI. I belive that the APIs to code UI on Apple's platforms can be really expressive, and reviewing a code change is way easier than trying to interpret the raw XML that Xcode generates for NIBs and Storyboards.
- What's great about a UI implemented in pure code is that it's also way more flexible and efficient, but it doesn't prevent the developers to use NIBs or Storyboards if it's required later down the road.
- I'm usign UserDefaults to store everything. For this specific exercise, I thought it'd be an overkill to add CoreData, Realm, or SQL stores to the application. However, I created clear abstractions for data persistance so that, in the case we would like to add a more robust DB engine, the changes are self contained and the rest of the app can keep on working, as long as the API contracts are honored.
- I'm relying on Swift 4's Codable to manage data encoding and decoding for the models.

### Improving further

If I had more time to put into this project, I'd like to...

- invest more time in the error handling paths of the application. Right now errors bubble up, but are not really handled/interpreted.
- study Twist's API a little bit more so I can be more informed when making decissions about app architecture.
- add a comprehensive test suite.
- create a build phase that, depending on the target configuration that's beign built right now, makes the app point to the right environment. (eg. dev points to staging, release to production)
- invest more time in adding platform features, such as keyboard commands, to make the app feel right at home.
- try to abstract as much platform-agnostic code as possible to make the iOS version of this app easier to build down the road.
