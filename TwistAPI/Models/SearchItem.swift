//
//  SearchItem.swift
//  TwistAPI
//
//  Created by Oscar Swanros on 12/28/17.
//  Copyright © 2017 Doist. All rights reserved.
//

public struct SearchItem: Codable {
    public enum ItemType: String, Codable {
        case thread
        case conversation
    }
    
    public let title: String?
    public let type: ItemType
    public let snippet: String
    
    public enum CodingKeys: String, CodingKey {
        case type = "obj_type"
        
        case title
        case snippet
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: SearchItem.CodingKeys.self)
        
        title = try container.decode(Optional<String>.self, forKey: .title)
        type = try container.decode(ItemType.self, forKey: .type)
        snippet = try container.decode(String.self, forKey: .snippet)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: SearchItem.CodingKeys.self)
        
        try container.encode(title, forKey: .title)
        try container.encode(snippet, forKey: .snippet)
        try container.encode(type, forKey: .type)
    }
}
