//
//  UserSession.swift
//  TwistAPI
//
//  Created by Oscar Swanros on 12/28/17.
//  Copyright © 2017 Doist. All rights reserved.
//

public struct UserSession: Codable {
//    public let id: Int
    public let email: String
    public let token: String
}
