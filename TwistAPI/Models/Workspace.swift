//
//  Workspace.swift
//  TwistAPI
//
//  Created by Oscar Swanros on 12/28/17.
//  Copyright © 2017 Doist. All rights reserved.
//

public struct Workspace: Codable {
    public let name: String
    public let id: Int
}

