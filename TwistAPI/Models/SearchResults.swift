//
//  SearchResults.swift
//  TwistAPI
//
//  Created by Oscar Swanros on 12/28/17.
//  Copyright © 2017 Doist. All rights reserved.
//

public struct SearchResults: Codable {
    public let items: [SearchItem]
    public var term: String = ""
    
    private enum CodingKeys: String, CodingKey {
        case items
    }
    
    public init(term: String, items: [SearchItem]) {
        self.term = term
        self.items = items
    }
    
    public func set(term: String) -> SearchResults {
        return SearchResults(term: term, items: items)
    }
}
