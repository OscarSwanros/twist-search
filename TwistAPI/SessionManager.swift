//
//  SessionManager.swift
//  TwistAPI
//
//  Created by Oscar Swanros on 12/28/17.
//  Copyright © 2017 Doist. All rights reserved.
//

private let kSessionDataKey = "Session_Data_Key"

public final class SessionManager {
    public static let shared = SessionManager()

    public var currentSession: UserSession? {
        guard
            let data = TwistAPI.defaults.data(forKey: kSessionDataKey),
            let session = try? TwistAPI.decoder.decode(UserSession.self, from: data)
            else {
                return nil
        }
        
        return session
    }
    
    public func save(session: UserSession) throws {
        let data = try TwistAPI.encoder.encode(session)
        
        TwistAPI.defaults.set(data, forKey: kSessionDataKey)
    }
    
    public func deleteCurrentSession() {
        TwistAPI.defaults.set(Data(), forKey: kSessionDataKey)
    }
}
