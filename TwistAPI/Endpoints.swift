//
//  Endpoints.swift
//  TwistAPI
//
//  Created by Oscar Swanros on 12/28/17.
//  Copyright © 2017 Doist. All rights reserved.
//

private let ST_API_BASE = "https://staging.twistapp.com/api/v2"
private let PR_API_BASE = "https://api.twistapp.com/api/v2"

public enum TwistEndpoint: String, EndpointConvertible {
    public enum Environment {
        case staging
        case production
    }
    
    public static var environment: Environment = .production
    
    case login      = "users/login"
    case workspaces = "workspaces/get"
    case search     = "search/query"
    
    var baseURLString: String {
        switch TwistEndpoint.environment {
        case .production:
            return PR_API_BASE
            
        case .staging:
            return ST_API_BASE
        }
    }
    
    func toURL() throws -> URL {
        guard let url = URL(string: "\(baseURLString)/\(self.rawValue)") else {
            throw EndpointError.invalidURL
        }
        
        return url
    }
    
    func toURL(params: [String : String]) throws -> URL {
        let encodedParams = params.toURLEncodedString()
        
        guard let url = URL(string: "\(baseURLString)/\(self.rawValue)?\(encodedParams)") else {
            throw EndpointError.invalidURL
        }
        
        return url
    }
}
