//
//  SearchResultsController.swift
//  TwistAPI
//
//  Created by Oscar Swanros on 12/28/17.
//  Copyright © 2017 Doist. All rights reserved.
//

private let kSearchResultsDataKey = "Search_Results_Data_Key"

public final class SearchResultsController {
    public static let shared = SearchResultsController()
    
    public var lastResults: SearchResults? {
        guard
            let data = TwistAPI.defaults.data(forKey: kSearchResultsDataKey),
            let s = try? TwistAPI.decoder.decode(SearchResults.self, from: data)
            else {
                return nil
        }
        
        return s
    }
    
    public func save(results: SearchResults) throws {
        let data = try TwistAPI.encoder.encode(results)
        
        TwistAPI.defaults.set(data, forKey: kSearchResultsDataKey)
    }
    
    public func deleteAll() {
        TwistAPI.defaults.set(Data(), forKey: kSearchResultsDataKey)
    }
}
