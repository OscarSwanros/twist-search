//
//  Dictionary.swift
//  TwistAPI
//
//  Created by Oscar Swanros on 12/28/17.
//  Copyright © 2017 Doist. All rights reserved.
//


extension Dictionary where Key: ExpressibleByStringLiteral, Value: ExpressibleByStringLiteral {
    public func toURLEncodedString() -> String {
        var pairs = [String]()
        for element in self {
            if
                let key = TwistAPI.encode(element.0 as AnyObject),
                let value = TwistAPI.encode(element.1 as AnyObject), (!value.isEmpty && !key.isEmpty) {
                pairs.append([key, value].joined(separator: "="))
            } else {
                continue
            }
        }
        
        guard !pairs.isEmpty else {
            return ""
        }
        
        return pairs.joined(separator: "&")
    }
}
