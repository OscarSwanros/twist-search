//
//  SearchTermOperation.swift
//  TwistAPI
//
//  Created by Oscar Swanros on 12/28/17.
//  Copyright © 2017 Doist. All rights reserved.
//

public final class SearchTermOperation: BaseNetworkOperation<SearchResults> {
    override var endpoint: EndpointConvertible {
        return TwistEndpoint.search
    }
    
    override var params: [String : String]? {
        return ["query": term]
    }
    
    private let term: String
    
    public var results: SearchResults?
    
    public init(term: String) {
        self.term = term
        
        super.init()
    }
    
    public override func didGenerate(model: SearchResults) {
        try? SearchResultsController.shared.save(results: model)
        
        results = model.set(term: self.term)
    }
}
