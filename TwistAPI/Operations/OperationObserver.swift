//
//  OperationObserver.swift
//  TwistAPI
//
//  Created by Oscar Swanros on 12/28/17.
//  Copyright © 2017 Doist. All rights reserved.
//

public protocol OperationObserver {
    func operationDidFinish(operation: TOperation)
    func operation(operation: TOperation, didFinishWithErrors errors: [Error])
}
