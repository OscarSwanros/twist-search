//
//  TOperation.swift
//  TwistAPI
//
//  Created by Oscar Swanros on 12/28/17.
//  Copyright © 2017 Doist. All rights reserved.
//

open class TOperation: Operation {
    public enum State {
        case executing
        case finished
        case finishedWithErrors
        case awaiting
    }
    
    public var state: State = .awaiting {
        didSet {
            willChangeValue(forKey: "isFinished")
            switch state {
            case .finished:
                for observer in observers {
                    observer.operationDidFinish(operation: self)
                }
            case .finishedWithErrors:
                for observer in observers {
                    observer.operation(operation: self, didFinishWithErrors: errors)
                }
                
            default: break
            }
            didChangeValue(forKey: "isFinished")
        }
    }
    
    public var errors: [Error] = []
    
    public var observers: [OperationObserver] = []
    
    override open var isFinished: Bool {
        return state == .finished || state == .finishedWithErrors
    }
    
    override open func main() {
        state = .executing
        execute()
    }
    
    open func execute(){
        fatalError("Override execute on TOperation subclasses.")
    }
    
    func finish() {
        state = .finished
    }
    
    func finish(errors: [Error]) {
        self.errors = errors
        state = .finishedWithErrors
    }
    
    public func add(observer: OperationObserver) {
        self.observers.append(observer)
    }
}

