//
//  LogInOperation.swift
//  TwistAPI
//
//  Created by Oscar Swanros on 12/28/17.
//  Copyright © 2017 Doist. All rights reserved.
//

public final class LogInOperation: BaseNetworkOperation<UserSession> {
    override var endpoint: EndpointConvertible {
        return TwistEndpoint.login
    }
    
    override var method: HTTPMethod {
        return .post
    }
    
    override var body: [String : Any]? {
        return [
            "email": email,
            "password": password
        ]
    }
    
    private let email: String
    private let password: String
    
    public init(email: String, password: String) {
        self.email = email
        self.password = password
        
        super.init()
    }
}
