//
//  BaseNetworkOperation.swift
//  TwistAPI
//
//  Created by Oscar Swanros on 12/28/17.
//  Copyright © 2017 Doist. All rights reserved.
//

enum HTTPMethod: String {
    case get    = "GET"
    case post   = "POST"
}


open class BaseNetworkOperation<T: Codable>: TOperation {
    var endpoint: EndpointConvertible {
        return NullEndpoint()
    }
    
    var method: HTTPMethod {
        return .get
    }
    
    var params: [String:String]? {
        return nil
    }
    
    var body: [String:Any]? {
        return nil
    }
    
    var headers: [String:String]? {
        return nil
    }
    
    public var generatedModel: T?
    
    open override func execute() {
        let getURL = { () -> URL? in
            if let params = self.params, self.method == .get {
                return try? self.endpoint.toURL(params: params)
            }
            
            return try? self.endpoint.toURL()
        }
        
        guard let url = getURL() else {
            state = .finished
            return
        }
        
        let session = URLSession(configuration: .ephemeral)
        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 3000)
        request.httpMethod = method.rawValue
        
        if method == .post {
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            
            if let body = body, let json = try? JSONSerialization.data(withJSONObject: body, options: .prettyPrinted) {
                request.httpBody = json
            }
        }
        
        if let headers = headers {
            for header in headers {
                request.setValue(header.1, forHTTPHeaderField: header.0)
            }
        }
        
        // If there's a session active, we need to pass the token
        if let s = SessionManager.shared.currentSession {
            request.setValue("Bearer \(s.token)", forHTTPHeaderField: "Authorization")
        }
        
        let task = session.dataTask(with: request) { (data, _, error) in
            guard let data = data, error == nil else {
                if let e = error {
                    self.finish(errors: [e])
                } else {
                    self.finish()
                }
                
                return
            }
            
            self.finishedDownloadingData(data: data)
        }
        
        task.resume()
    }
    
    func finishedDownloadingData(data: Data) {
        let decoder = JSONDecoder()
        
        do {
            let model = try decoder.decode(T.self, from: data)
            
            generatedModel = model
            didGenerate(model: model)
            finish()
        } catch {
          finish(errors: [error])
        }
    }
    
    open func didGenerate(model: T) {
        // For subclassing
    }
}
