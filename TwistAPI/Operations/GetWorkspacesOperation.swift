//
//  GetWorkspacesOperation.swift
//  TwistAPI
//
//  Created by Oscar Swanros on 12/28/17.
//  Copyright © 2017 Doist. All rights reserved.
//

public final class GetWorkspacesOperation: BaseNetworkOperation<[Workspace]> {
    override var endpoint: EndpointConvertible {
        return TwistEndpoint.workspaces
    }
}
