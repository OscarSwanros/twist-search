//
//  TwistAPI.h
//  TwistAPI
//
//  Created by Oscar Swanros on 12/28/17.
//  Copyright © 2017 Doist. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for TwistAPI.
FOUNDATION_EXPORT double TwistAPIVersionNumber;

//! Project version string for TwistAPI.
FOUNDATION_EXPORT const unsigned char TwistAPIVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <TwistAPI/PublicHeader.h>


