//
//  EndpointConvertible.swift
//  TwistAPI
//
//  Created by Oscar Swanros on 12/28/17.
//  Copyright © 2017 Doist. All rights reserved.
//

public enum EndpointError: Error {
    case invalidURL
}

protocol EndpointConvertible {
    var baseURLString: String { get }
    
    func toURL() throws -> URL
    func toURL(params: [String:String]) throws -> URL
}

struct NullEndpoint: EndpointConvertible {
    
    var baseURLString: String {
        return "https://httpbin.org"
    }
    
    func toURL() throws -> URL {
        throw EndpointError.invalidURL
    }
    
    func toURL(params: [String : String]) throws -> URL {
        throw EndpointError.invalidURL
    }
}
