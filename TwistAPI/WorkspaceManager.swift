//
//  WorkspaceManager.swift
//  TwistAPI
//
//  Created by Oscar Swanros on 12/28/17.
//  Copyright © 2017 Doist. All rights reserved.
//

private let kWorkspacesDataKey = "Workspaces_Data_Key"

public extension Notification.Name {
    static let workspacesStoreUpdated = Notification.Name(rawValue: "workspacesStoreUpdated")
}

public final class WorkspaceManager {
    public static let shared = WorkspaceManager()

    public var currentWorkspace: Workspace? {
        return workspaces?.first
    }
    
    public var workspaces: [Workspace]? {
        guard
            let data = TwistAPI.defaults.data(forKey: kWorkspacesDataKey),
            let w = try? TwistAPI.decoder.decode([Workspace].self, from: data)
            else {
                return nil
        }
        
        return w
    }
    
    public func save(workspaces: [Workspace]) throws {
        let data = try TwistAPI.encoder.encode(workspaces)
        
        TwistAPI.defaults.set(data, forKey: kWorkspacesDataKey)
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: .workspacesStoreUpdated, object: nil)
            
        }
    }
    
    public func deleteWorkspaces() {
        TwistAPI.defaults.set(Data(), forKey: kWorkspacesDataKey)
    }
}
