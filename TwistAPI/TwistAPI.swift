//
//  TwistAPI.swift
//  TwistAPI
//
//  Created by Oscar Swanros on 12/28/17.
//  Copyright © 2017 Doist. All rights reserved.
//

public struct TwistAPI {
    static let defaults = UserDefaults.standard
    
    static var decoder: JSONDecoder {
        let d = JSONDecoder()
        
        return d
    }
    
    static var encoder: JSONEncoder {
        let e = JSONEncoder()
        
        return e
    }
    
    static func encode(_ o: Any) -> String? {
        guard let string = o as? NSString else {
            return nil
        }
        
        return string.removingPercentEncoding
    }
}
